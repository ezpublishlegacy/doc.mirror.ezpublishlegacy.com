Message-ID: <1137955989.782.1413881607240.JavaMail.confluence@ip-10-127-227-192>
Subject: Exported From Confluence
MIME-Version: 1.0
Content-Type: multipart/related; 
	boundary="----=_Part_781_908534937.1413881607240"

------=_Part_781_908534937.1413881607240
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable
Content-Location: file:///C:/exported.html

<html xmlns:o=3D'urn:schemas-microsoft-com:office:office'
      xmlns:w=3D'urn:schemas-microsoft-com:office:word'
      xmlns:v=3D'urn:schemas-microsoft-com:vml'
      xmlns=3D'urn:w3-org-ns:HTML'>
<head>
    <meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Dutf-8=
">
    <title>Authentication</title>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:TargetScreenSize>1024x640</o:TargetScreenSize>
            <o:PixelsPerInch>72</o:PixelsPerInch>
            <o:AllowPNG/>
        </o:OfficeDocumentSettings>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
    <![endif]-->
    <style>
                <!--
        @page Section1 {
            size: 8.5in 11.0in;
            margin: 1.0in;
            mso-header-margin: .5in;
            mso-footer-margin: .5in;
            mso-paper-source: 0;
        }

        td {
            page-break-inside: avoid;
        }

        tr {
            page-break-after: avoid;
        }

        div.Section1 {
            page: Section1;
        }

        /* Confluence print stylesheet. Common to all themes for print medi=
a */
/* Full of !important until we improve batching for print CSS */

#main {
    padding-bottom: 1em !important; /* The default padding of 6em is too mu=
ch for printouts */
}

body {
    font-family: Arial, Helvetica, FreeSans, sans-serif;
    font-size: 10pt;
    line-height: 1.2;
}

body, #full-height-container, #main, #page, #content, .has-personal-sidebar=
 #content {
    background: #fff    !important;
    color: #000         !important;
    border: 0           !important;
    width: 100%         !important;
    height: auto        !important;
    min-height: auto    !important;
    margin: 0           !important;
    padding: 0          !important;
    display: block      !important;
}

a, a:link, a:visited, a:focus, a:hover, a:active {
    color: #000;
}
#content h1,
#content h2,
#content h3,
#content h4,
#content h5,
#content h6 {
    font-family: Arial, Helvetica, FreeSans, sans-serif;
    page-break-after: avoid;
}
pre {
    font-family: Monaco, "Courier New", monospace;
}

#header,
.aui-header-inner,
#navigation,
#sidebar,
.sidebar,
#personal-info-sidebar,
.ia-fixed-sidebar,
.page-actions,
.navmenu,
.ajs-menu-bar,
.noprint,
.inline-control-link,
.inline-control-link a,
a.show-labels-editor,
.global-comment-actions,
.comment-actions,
.quick-comment-container,
#addcomment {
    display: none !important;
}

.comment .date::before {
    content: none !important; /* remove middot for print view */
}

h1.pagetitle img {
    height: auto;
    width: auto;
}

.print-only {
    display: block;
}
#footer {
    position: relative !important; /* CONF-17506 Place the footer at end of=
 the content */
    margin: 0;
    padding: 0;
    background: none;
    clear: both;
}

#poweredby {
    border-top: none;
    background: none;
}

#poweredby li.print-only {
    display: list-item;
    font-style: italic;
}

#poweredby li.noprint {
    display:none;
}


/* no width controls in print */
.wiki-content .table-wrap,
.wiki-content p,
.panel .codeContent,
.panel .codeContent pre,
.image-wrap {
    overflow: visible !important;
}

/* TODO - should this work? */
#children-section,
#comments-section .comment,
#comments-section .comment .comment-body,
#comments-section .comment .comment-content,
#comments-section .comment p {
    page-break-inside: avoid;
}

#page-children a {
    text-decoration: none;
}

/**
 hide twixies

 the specificity here is a hack because print styles
 are getting loaded before the base styles. */
#comments-section.pageSection .section-header,
#comments-section.pageSection .section-title,
#children-section.pageSection .section-header,
#children-section.pageSection .section-title,
.children-show-hide {
    padding-left: 0;
    margin-left: 0;
}

.children-show-hide.icon {
    display: none;
}

/* personal sidebar */
.has-personal-sidebar #content {
    margin-right: 0px;
}

.has-personal-sidebar #content .pageSection {
    margin-right: 0px;
}
-->
    </style>
</head>
<body>
    <h1>Authentication</h1>
    <div class=3D"Section1">
        <p>(&gt;=3D EZP 5.3, &gt;=3D EZP Community 2014.01)</p>
<p><style type=3D"text/css">/*<![CDATA[*/
div.rbtoc1413881607202 {padding: 0px;}
div.rbtoc1413881607202 ul {list-style: disc;margin-left: 0px;}
div.rbtoc1413881607202 li {margin-left: 0px;padding-left: 0px;}

/*]]>*/</style></p>
<div class=3D"toc-macro rbtoc1413881607202">=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#Authentication-AuthenticationusingSymfonySecuritycomponent"=
>Authentication using Symfony Security component</a>=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#Authentication-Securitycontroller">Security controller</a>=
=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#Authentication-Redirectionafterlogin">Redirection after log=
in</a></li>=20
</ul> </li>=20
<li><a href=3D"#Authentication-Configuration">Configuration</a></li>=20
<li><a href=3D"#Authentication-Accesscontrol">Access control</a></li>=20
<li><a href=3D"#Authentication-Rememberme">Remember me</a></li>=20
<li><a href=3D"#Authentication-Loginhandlers/SSO">Login handlers / SSO</a><=
/li>=20
<li><a href=3D"#Authentication-IntegrationwithLegacy">Integration with Lega=
cy</a></li>=20
</ul> </li>=20
<li><a href=3D"#Authentication-AuthenticationwithLegacySSOHandlers">Authent=
ication with Legacy SSO Handlers</a></li>=20
<li><a href=3D"#Authentication-Upgradenotes">Upgrade notes</a></li>=20
</ul>=20
</div>
<p></p>=20
<div class=3D"aui-message warning shadowed information-macro">=20
<p class=3D"title">Version compatibility</p>=20
<span class=3D"aui-icon icon-warning">Icon</span>=20
<div class=3D"message-content">=20
<p>This documentation page is compatible with&nbsp;<strong>eZ Publish 5.3 /=
 2014.01</strong></p>
<p><span>Prior to these versions, authentication was made through legacy st=
ack only, using the venerable&nbsp;</span><code>user/login</code><span>&nbs=
p;module, with the help of a&nbsp;</span><code>PreAuthenticatedProvider</co=
de><span>.</span></p>=20
</div>=20
</div>=20
<h2 id=3D"Authentication-AuthenticationusingSymfonySecuritycomponent">Authe=
ntication using Symfony Security component</h2>
<p><span>&nbsp;</span><a href=3D"http://symfony.com/doc/2.3/book/security.h=
tml#using-a-traditional-login-form" class=3D"external-link" rel=3D"nofollow=
"><span>Native and universal&nbsp;</span><code>form_login</code></a><span>&=
nbsp;is used, in conjunction to an extended&nbsp;</span><code>DaoAuthentica=
tionProvider</code><span>&nbsp;(DAO stands for&nbsp;</span><em>Data Access =
Object</em><span>), the&nbsp;</span><code>RepositoryAuthenticationProvider<=
/code><span>. Native behavior of&nbsp;</span><code>DaoAuthenticationProvide=
r</code><span>&nbsp;has been preserved, making it possible to still use it =
for pure Symfony applications.</span></p>
<h3 id=3D"Authentication-Securitycontroller"><span>Security controller</spa=
n></h3>
<p>A&nbsp;<code>SecurityController</code>&nbsp;is used to manage all securi=
ty related actions and is thus used to display login form. It is pretty str=
aight forward and follows all standards explained in&nbsp;<a href=3D"http:/=
/symfony.com/doc/2.3/book/security.html#using-a-traditional-login-form" cla=
ss=3D"external-link" rel=3D"nofollow">Symfony security documentation</a>.</=
p>
<p>Base template used is&nbsp;<code>EzPublishCoreBundle:Security:login.html=
.twig</code>&nbsp;and stands as follows:</p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: xml; gutter: false" style=3D"font-size=
:12px;">{% extends layout %}

{% block content %}
    {% block login_content %}
        {% if error %}
            &lt;div&gt;{{ error.message|trans }}&lt;/div&gt;
        {% endif %}

        &lt;form action=3D&quot;{{ path( 'login_check' ) }}&quot; method=3D=
&quot;post&quot;&gt;
        {% block login_fields %}
            &lt;label for=3D&quot;username&quot;&gt;{{ 'Username:'|trans }}=
&lt;/label&gt;
            &lt;input type=3D&quot;text&quot; id=3D&quot;username&quot; nam=
e=3D&quot;_username&quot; value=3D&quot;{{ last_username }}&quot; /&gt;

            &lt;label for=3D&quot;password&quot;&gt;{{ 'Password:'|trans }}=
&lt;/label&gt;
            &lt;input type=3D&quot;password&quot; id=3D&quot;password&quot;=
 name=3D&quot;_password&quot; /&gt;

            &lt;input type=3D&quot;hidden&quot; name=3D&quot;_csrf_token&qu=
ot; value=3D&quot;{{ csrf_token }}&quot; /&gt;

            {#
                If you want to control the URL the user
                is redirected to on success (more details below)
                &lt;input type=3D&quot;hidden&quot; name=3D&quot;_target_pa=
th&quot; value=3D&quot;/account&quot; /&gt;
            #}

            &lt;button type=3D&quot;submit&quot;&gt;{{ 'Login'|trans }}&lt;=
/button&gt;
        {% endblock %}
        &lt;/form&gt;
    {% endblock %}
{% endblock %}</pre>=20
</div>
</div>
<p><span><span>The layout used by default is&nbsp;</span><code>%ezpublish.c=
ontent_view.viewbase_layout%</code><span>&nbsp;(empty layout) but can be co=
nfigured easily as well as the login template:</span></span></p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeHeader panelHeader pdl" style=3D"border-bottom-width: 1px=
;">
<b>ezpublish.yml</b>
</div>
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: php; gutter: false" style=3D"font-size=
:12px;">ezpublish:
    system:
        my_siteaccess:
            user:
                layout: &quot;AcmeTestBundle::layout.html.twig&quot;
                login_template: &quot;AcmeTestBundle:User:login.html.twig&q=
uot;</pre>=20
</div>
</div>
<h4 id=3D"Authentication-Redirectionafterlogin">Redirection after login</h4=
>
<p>By default, Symfony redirects to the&nbsp;<a href=3D"http://symfony.com/=
doc/2.3/reference/configuration/security.html" class=3D"external-link" rel=
=3D"nofollow">URI configured in&nbsp;<code>security.yml</code>&nbsp;as&nbsp=
;<code>default_target_path</code></a>. If not set, it will default to&nbsp;=
<code>/</code>.</p>
<p>This setting can be set by SiteAccess, via&nbsp;<a href=3D"/display/EZP/=
EzPublishCoreBundle+Configuration#EzPublishCoreBundleConfiguration-EzPublis=
hCoreBundleConfiguration-Defaultpage"><code>default_page</code>&nbsp;settin=
g</a>.</p>
<h3 id=3D"Authentication-Configuration">Configuration</h3>
<p><span>To use Symfony authentication with eZ Publish, the configuration g=
oes as follows:</span></p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeHeader panelHeader pdl" style=3D"border-bottom-width: 1px=
;">
<b>ezpublish/config/security.yml</b>
</div>
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: php; gutter: false" style=3D"font-size=
:12px;">security:
    firewalls:
        ezpublish_front:
            pattern: ^/
            anonymous: ~
            form_login:
                require_previous_session: false
            logout: ~</pre>=20
</div>
</div>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeHeader panelHeader pdl" style=3D"border-bottom-width: 1px=
;">
<b>ezpublish/config/routing.yml</b>
</div>
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: php; gutter: false" style=3D"font-size=
:12px;">login:
    path:   /login
    defaults:  { _controller: ezpublish.security.controller:loginAction }
login_check:
    path:   /login_check
logout:
    path:   /logout</pre>=20
</div>
</div>=20
<div class=3D"aui-message hint shadowed information-macro">=20
<p class=3D"title">Note</p>=20
<span class=3D"aui-icon icon-hint">Icon</span>=20
<div class=3D"message-content">=20
<p>You can fully customize the routes and/or the controller used for login.=
 However, ensure to match <code>login_path</code>, <code>check_path</code> =
and logout.path from <code>security.yml</code>.</p>
<p>See <a href=3D"http://symfony.com/doc/2.3/reference/configuration/securi=
ty.html" class=3D"external-link" rel=3D"nofollow">security configuration re=
ference</a> and <a href=3D"http://symfony.com/doc/2.3/book/security.html#us=
ing-a-traditional-login-form" class=3D"external-link" rel=3D"nofollow">stan=
dard login form documentation</a>.</p>=20
</div>=20
</div>=20
<p>&nbsp;</p>
<h3 id=3D"Authentication-Accesscontrol">Access control</h3>
<p>See the <a href=3D"http://doc.ez.no/eZ-Publish/Technical-manual/5.x/Conc=
epts-and-basics/Access-control" class=3D"external-link" rel=3D"nofollow">do=
cumentation on access control</a></p>
<h3 id=3D"Authentication-Rememberme">Remember me</h3>
<p>It is possible to use the&nbsp;<code>remember_me</code>&nbsp;functionali=
ty. For this you can refer to the&nbsp;<a href=3D"http://symfony.com/doc/2.=
3/cookbook/security/remember_me.html" class=3D"external-link" rel=3D"nofoll=
ow">Symfony cookbook on this topic</a>.</p>
<p>If you want to use this feature, you must at least extend the login temp=
late in order to add the required checkbox:</p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: xml; gutter: false" style=3D"font-size=
:12px;">{# your_login_template.html.twig #}
{% extends &quot;EzPublishCoreBundle:Security:login.html.twig&quot; %}

{% block login_fields %}
    {{ parent() }}
    &lt;input type=3D&quot;checkbox&quot; id=3D&quot;remember_me&quot; name=
=3D&quot;_remember_me&quot; checked /&gt;
    &lt;label for=3D&quot;remember_me&quot;&gt;Keep me logged in&lt;/label&=
gt;
{% endblock %}</pre>=20
</div>
</div>
<h3 id=3D"Authentication-Loginhandlers/SSO">Login handlers / SSO</h3>
<p>Symfony provides native support for&nbsp;<a href=3D"http://symfony.com/d=
oc/2.3/book/security.html#using-multiple-user-providers" class=3D"external-=
link" rel=3D"nofollow">multiple user providers</a>. This makes it easy to i=
ntegrate any kind of login handlers, including SSO and existing 3rd party b=
undles (e.g.&nbsp;<a href=3D"https://github.com/Maks3w/FR3DLdapBundle" clas=
s=3D"external-link" rel=3D"nofollow">FR3DLdapBundle</a>,&nbsp;<a href=3D"ht=
tps://github.com/hwi/HWIOAuthBundle" class=3D"external-link" rel=3D"nofollo=
w">HWIOauthBundle</a>,&nbsp;<a href=3D"https://github.com/FriendsOfSymfony/=
FOSUserBundle" class=3D"external-link" rel=3D"nofollow">FOSUserBundle</a>,&=
nbsp;<a href=3D"http://github.com/BeSimple/BeSimpleSsoAuthBundle" class=3D"=
external-link" rel=3D"nofollow">BeSimpleSsoAuthBundle</a>...).</p>
<p>Further explanation can be found in the&nbsp;<a href=3D"/display/EZP/How=
+to+authenticate+a+user+with+multiple+user+providers">multiple user provide=
rs cookbook entry</a>.</p>
<h3 id=3D"Authentication-IntegrationwithLegacy">Integration with Legacy</h3=
>
<ul>
<li>When&nbsp;<strong>not</strong>&nbsp;in legacy mode, legacy&nbsp;<code>u=
ser/login</code>&nbsp;and&nbsp;<code>user/logout</code>&nbsp;views are deac=
tivated.</li>
<li>Authenticated user is injected in legacy kernel.</li>
</ul>
<h2 id=3D"Authentication-AuthenticationwithLegacySSOHandlers">Authenticatio=
n with Legacy SSO Handlers</h2>
<p>To be able to use your legacy SSO (Single Sign-on) handlers, use the fol=
lowing config in your <code>ezpublish/config/security.yml</code>:</p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeHeader panelHeader pdl" style=3D"border-bottom-width: 1px=
;">
<b>Use your legacy SSO handlers</b>
</div>
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: php; gutter: false" style=3D"font-size=
:12px;">security:
    firewalls:
        ezpublish_front:
            pattern: ^/
            anonymous: ~
            # Adding the following entry will activate the use of old SSO h=
andlers.
            ezpublish_legacy_sso: ~ </pre>=20
</div>
</div>=20
<div class=3D"aui-message success shadowed information-macro">=20
<span class=3D"aui-icon icon-success">Icon</span>=20
<div class=3D"message-content">
 If you need to
<a href=3D"http://share.ez.no/learn/ez-publish/using-a-sso-in-ez-publish" c=
lass=3D"external-link" rel=3D"nofollow"> create your legacy SSO Handler, pl=
ease read this entry</a>=20
</div>=20
</div>=20
<h2 id=3D"Authentication-Upgradenotes">Upgrade notes</h2>=20
<div class=3D"aui-message success shadowed information-macro">=20
<span class=3D"aui-icon icon-success">Icon</span>=20
<div class=3D"message-content">=20
<p>Follow the notes below if you upgrade from 5.2 to 5.3 / 2013.11 to 2014.=
01</p>=20
</div>=20
</div>=20
<ul>
<li>In&nbsp;<code>ezpublish/config/security.yml</code>, you must remove&nbs=
p;<code>ezpublish: true</code>&nbsp;from&nbsp;<code>ezpublish_front</code>&=
nbsp;firewall.</li>
<li>In&nbsp;<code>ezpublish/config/routing.yml</code>, you must add&nbsp;<c=
ode>login</code>,&nbsp;<code>login_check</code>&nbsp;and&nbsp;<code>logout<=
/code>&nbsp;routes (see above in [Configuration][])</li>
<li>In your templates, change your links pointing to&nbsp;<code>/user/login=
</code>&nbsp;and&nbsp;<code>/user/logout</code>&nbsp;to appropriate login/l=
ogin_check/logout routes:</li>
</ul>
<p><em>Before:</em></p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: xml; gutter: false" style=3D"font-size=
:12px;">&lt;a href=3D&quot;{{ path( 'ez_legacy', {'module_uri': '/user/logi=
n'} ) }}&quot;&gt;Login&lt;/a&gt;

&lt;form action=3D&quot;{{ path( 'ez_legacy', {'module_uri': '/user/login'}=
 ) }}&quot; method=3D&quot;post&quot;&gt;

&lt;a href=3D&quot;{{ path( 'ez_legacy', {'module_uri': '/user/logout'} ) }=
}&quot;&gt;Logout&lt;/a&gt;</pre>=20
</div>
</div>
<p><em>After:</em></p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: xml; gutter: false" style=3D"font-size=
:12px;">&lt;a href=3D&quot;{{ path( 'login' ) }}&quot;&gt;Login&lt;/a&gt;

&lt;form action=3D&quot;{{ path( 'login_check' ) }}&quot; method=3D&quot;po=
st&quot;&gt;

&lt;a href=3D&quot;{{ path( 'logout' ) }}&quot;&gt;Logout&lt;/a&gt;</pre>=
=20
</div>
</div>
    </div>
</body>
</html>
------=_Part_781_908534937.1413881607240--
