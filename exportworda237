Message-ID: <877449212.1428.1413895904493.JavaMail.confluence@ip-10-127-227-192>
Subject: Exported From Confluence
MIME-Version: 1.0
Content-Type: multipart/related; 
	boundary="----=_Part_1427_1826952974.1413895904493"

------=_Part_1427_1826952974.1413895904493
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable
Content-Location: file:///C:/exported.html

<html xmlns:o=3D'urn:schemas-microsoft-com:office:office'
      xmlns:w=3D'urn:schemas-microsoft-com:office:word'
      xmlns:v=3D'urn:schemas-microsoft-com:vml'
      xmlns=3D'urn:w3-org-ns:HTML'>
<head>
    <meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Dutf-8=
">
    <title>Legacy configuration injection</title>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:TargetScreenSize>1024x640</o:TargetScreenSize>
            <o:PixelsPerInch>72</o:PixelsPerInch>
            <o:AllowPNG/>
        </o:OfficeDocumentSettings>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
    <![endif]-->
    <style>
                <!--
        @page Section1 {
            size: 8.5in 11.0in;
            margin: 1.0in;
            mso-header-margin: .5in;
            mso-footer-margin: .5in;
            mso-paper-source: 0;
        }

        td {
            page-break-inside: avoid;
        }

        tr {
            page-break-after: avoid;
        }

        div.Section1 {
            page: Section1;
        }

        /* Confluence print stylesheet. Common to all themes for print medi=
a */
/* Full of !important until we improve batching for print CSS */

#main {
    padding-bottom: 1em !important; /* The default padding of 6em is too mu=
ch for printouts */
}

body {
    font-family: Arial, Helvetica, FreeSans, sans-serif;
    font-size: 10pt;
    line-height: 1.2;
}

body, #full-height-container, #main, #page, #content, .has-personal-sidebar=
 #content {
    background: #fff    !important;
    color: #000         !important;
    border: 0           !important;
    width: 100%         !important;
    height: auto        !important;
    min-height: auto    !important;
    margin: 0           !important;
    padding: 0          !important;
    display: block      !important;
}

a, a:link, a:visited, a:focus, a:hover, a:active {
    color: #000;
}
#content h1,
#content h2,
#content h3,
#content h4,
#content h5,
#content h6 {
    font-family: Arial, Helvetica, FreeSans, sans-serif;
    page-break-after: avoid;
}
pre {
    font-family: Monaco, "Courier New", monospace;
}

#header,
.aui-header-inner,
#navigation,
#sidebar,
.sidebar,
#personal-info-sidebar,
.ia-fixed-sidebar,
.page-actions,
.navmenu,
.ajs-menu-bar,
.noprint,
.inline-control-link,
.inline-control-link a,
a.show-labels-editor,
.global-comment-actions,
.comment-actions,
.quick-comment-container,
#addcomment {
    display: none !important;
}

.comment .date::before {
    content: none !important; /* remove middot for print view */
}

h1.pagetitle img {
    height: auto;
    width: auto;
}

.print-only {
    display: block;
}
#footer {
    position: relative !important; /* CONF-17506 Place the footer at end of=
 the content */
    margin: 0;
    padding: 0;
    background: none;
    clear: both;
}

#poweredby {
    border-top: none;
    background: none;
}

#poweredby li.print-only {
    display: list-item;
    font-style: italic;
}

#poweredby li.noprint {
    display:none;
}


/* no width controls in print */
.wiki-content .table-wrap,
.wiki-content p,
.panel .codeContent,
.panel .codeContent pre,
.image-wrap {
    overflow: visible !important;
}

/* TODO - should this work? */
#children-section,
#comments-section .comment,
#comments-section .comment .comment-body,
#comments-section .comment .comment-content,
#comments-section .comment p {
    page-break-inside: avoid;
}

#page-children a {
    text-decoration: none;
}

/**
 hide twixies

 the specificity here is a hack because print styles
 are getting loaded before the base styles. */
#comments-section.pageSection .section-header,
#comments-section.pageSection .section-title,
#children-section.pageSection .section-header,
#children-section.pageSection .section-title,
.children-show-hide {
    padding-left: 0;
    margin-left: 0;
}

.children-show-hide.icon {
    display: none;
}

/* personal sidebar */
.has-personal-sidebar #content {
    margin-right: 0px;
}

.has-personal-sidebar #content .pageSection {
    margin-right: 0px;
}
-->
    </style>
</head>
<body>
    <h1>Legacy configuration injection</h1>
    <div class=3D"Section1">
        <p>For better integration between 5.x (symfony based) kernel and le=
gacy (4.x) kernel, injection is used to inject settings, <a href=3D"/displa=
y/EZP51/Legacy+session+injection">session</a> and <a href=3D"/display/EZP51=
/Legacy+siteaccess+injection">current siteaccess</a> from 5.x into legacy u=
sing an event:&nbsp;<span class=3D"nv" style=3D"color: rgb(0,128,128);"><a =
href=3D"/display/EZP51/Legacy+kernel+event">kernel.event_subscriber</a></sp=
an></p>
<p><span class=3D"nv" style=3D"color: rgb(0,128,128);"><style type=3D"text/=
css">/*<![CDATA[*/
div.rbtoc1413895904457 {padding: 0px;}
div.rbtoc1413895904457 ul {list-style: disc;margin-left: 0px;}
div.rbtoc1413895904457 li {margin-left: 0px;padding-left: 0px;}

/*]]>*/</style></span></p>
<div class=3D"toc-macro rbtoc1413895904457">=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#Legacyconfigurationinjection-Injectedsettings">Injected set=
tings</a>=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#Legacyconfigurationinjection-Databasesettings">Database set=
tings</a></li>=20
<li><a href=3D"#Legacyconfigurationinjection-Storagesettings">Storage setti=
ngs</a></li>=20
<li><a href=3D"#Legacyconfigurationinjection-Imagesettings">Image settings<=
/a></li>=20
<li><a href=3D"#Legacyconfigurationinjection-UsingImageMagickfilters">Using=
 ImageMagick filters</a></li>=20
<li><a href=3D"#Legacyconfigurationinjection-Extendinginjectedsettings">Ext=
ending injected settings</a></li>=20
</ul> </li>=20
<li><a href=3D"#Legacyconfigurationinjection-InjectedBehavior">Injected&nbs=
p;Behavior</a>=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#Legacyconfigurationinjection-eZFormToken(CSRF)integration">=
eZFormToken (CSRF) integration</a></li>=20
<li><a href=3D"#Legacyconfigurationinjection-ezpEvent(Cacheclear)integratio=
n">ezpEvent (Cache clear) integration</a></li>=20
</ul> </li>=20
</ul>=20
</div>
<br />
<p></p>
<h2 id=3D"Legacyconfigurationinjection-Injectedsettings">Injected settings<=
/h2>
<p>Several settings are injected to avoid having to duplicate settings acro=
ss new and old kernel, this is done in&nbsp;eZ\Bundle\EzPublishLegacyBundle=
\LegacyMapper\Configuration.<br />For mappings below, yml settings prefixed=
 with ezpublish are system settings, while the rest are settings you can de=
fine pr siteaccess.</p>
<h3 id=3D"Legacyconfigurationinjection-Databasesettings">Database settings<=
/h3>
<p>The settings for &quot;Server&quot;, &quot;Port&quot;, &quot;User&quot;,=
 &quot;Password&quot;, &quot;Database&quot; and &quot;DatabaseImplementatio=
n&quot; is set based on the current settings in your yml files, either from=
 the explicit settings defined below, or the &quot;dsn&quot;</p>
<p><strong>Mapping</strong>:</p>
<div class=3D"table-wrap">
<table class=3D"confluenceTable">
<tbody>
<tr>
<th class=3D"confluenceTh">yml</th>
<th class=3D"confluenceTh">site.ini <span>[</span><span>DatabaseSettings]</=
span></th>
</tr>
<tr>
<td class=3D"confluenceTd">server</td>
<td class=3D"confluenceTd">Server</td>
</tr>
<tr>
<td class=3D"confluenceTd">port</td>
<td class=3D"confluenceTd">Port</td>
</tr>
<tr>
<td class=3D"confluenceTd">user</td>
<td class=3D"confluenceTd">User</td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">password</td>
<td colspan=3D"1" class=3D"confluenceTd">Password</td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">database_name</td>
<td colspan=3D"1" class=3D"confluenceTd">Database</td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">type</td>
<td colspan=3D"1" class=3D"confluenceTd">DatabaseImplementation</td>
</tr>
</tbody>
</table>
</div>
<p>&nbsp;</p>
<h3 id=3D"Legacyconfigurationinjection-Storagesettings">Storage settings</h=
3>
<p>The settings for &quot;VarDir&quot; and &quot;StorageDir&quot; is set ba=
sed on current settings in your yml files as shown below.</p>
<p><strong>Mapping:</strong></p>
<div class=3D"table-wrap">
<table class=3D"confluenceTable">
<tbody>
<tr>
<th class=3D"confluenceTh">yml</th>
<th class=3D"confluenceTh">site.ini [FileSettings]</th>
</tr>
<tr>
<td class=3D"confluenceTd">var_dir</td>
<td class=3D"confluenceTd">VarDir</td>
</tr>
<tr>
<td class=3D"confluenceTd">storage_dir</td>
<td class=3D"confluenceTd">StorageDir</td>
</tr>
</tbody>
</table>
</div>
<p>&nbsp;</p>
<h3 id=3D"Legacyconfigurationinjection-Imagesettings">Image settings</h3>
<p>Some of the settings for image sub system is set based on your yml files=
&nbsp;as shown below.</p>
<p><strong>Mapping:</strong></p>
<div class=3D"table-wrap">
<table class=3D"confluenceTable">
<tbody>
<tr>
<th class=3D"confluenceTh">yml</th>
<th class=3D"confluenceTh">image.ini</th>
</tr>
<tr>
<td class=3D"confluenceTd">ezpublish.image.temporary_dir</td>
<td class=3D"confluenceTd">[FileSettings]\TemporaryDir</td>
</tr>
<tr>
<td class=3D"confluenceTd">ezpublish.image.published_images_dir</td>
<td class=3D"confluenceTd"><span>[FileSettings]\PublishedImages</span></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">ezpublish.image.versioned_images_d=
ir</td>
<td colspan=3D"1" class=3D"confluenceTd"><span>[FileSettings]\VersionedImag=
es</span></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">image_variations</td>
<td colspan=3D"1" class=3D"confluenceTd"><span>[FileSettings]\AliasList</sp=
an></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">ezpublish.image.imagemagick.enable=
d</td>
<td colspan=3D"1" class=3D"confluenceTd"><span>[ImageMagick]\IsEnabled</spa=
n></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">ezpublish.image.imagemagick.execut=
able_path</td>
<td colspan=3D"1" class=3D"confluenceTd"><span>[ImageMagick]\ExecutablePath=
</span></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">ezpublish.image.imagemagick.execut=
able</td>
<td colspan=3D"1" class=3D"confluenceTd"><span>[ImageMagick]\Executable</sp=
an></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">imagemagick.pre_parameters</td>
<td colspan=3D"1" class=3D"confluenceTd"><span>[</span><span>ImageMagick</s=
pan><span>]\PreParameters</span></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">imagemagick.post_parameters</td>
<td colspan=3D"1" class=3D"confluenceTd"><span>[</span><span>ImageMagick</s=
pan><span>]\PostParameters</span></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd">ezpublish.image.imagemagick.filter=
s</td>
<td colspan=3D"1" class=3D"confluenceTd"><span>[</span><span>ImageMagick</s=
pan><span>]\Filters</span></td>
</tr>
</tbody>
</table>
</div>
<p>&nbsp;</p>
<h3 id=3D"Legacyconfigurationinjection-UsingImageMagickfilters">Using Image=
Magick filters</h3>
<p>The following block shows a valid ImageMagick filters usage example for =
<code>ezpublish.yml</code>:</p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeHeader panelHeader pdl" style=3D"border-bottom-width: 1px=
;">
<b>ImageMagick filters usage example</b>
</div>
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: php; gutter: false" style=3D"font-size=
:12px;">ezpublish:
    imagemagick:
        filters:
            geometry/scale: &quot;-geometry {1}x{2}&quot;</pre>=20
</div>
</div>
<p>Since ImageMagick filters usage changed from eZ Publish 4.x versions you=
 can find the list of filters existing by default to use eZ Publish 5.x:</p=
>
<div class=3D"table-wrap">
<table class=3D"confluenceTable">
<tbody>
<tr>
<th class=3D"confluenceTh">ImageMagick filters list for yml</th>
</tr>
<tr>
<td class=3D"confluenceTd"><code>sharpen: &quot;-sharpen 0.5&quot;</code></=
td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/scale: &quot;-geometry {1}x{2}&qu=
ot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/scalewidth: &quot;-geometry {1}&q=
uot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/scaleheight: &quot;-geometry x{1}=
&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/scaledownonly: &quot;-geometry {1=
}x{2}&gt;&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/scalewidthdownonly: &quot;-geomet=
ry {1}&gt;&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/scaleheightdownonly: &quot;-geome=
try x{1}&gt;&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/scaleexact: &quot;-geometry {1}x{=
2}!&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/scalepercent: &quot;-geometry {1}=
x{2}&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>geometry/crop: &quot;-crop {1}x{2}+{3}+{4}=
&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>filter/noise: &quot;-noise {1}&quot;</code=
></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>filter/swirl: &quot;-swirl {1}&quot;</code=
></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>colorspace/gray: &quot;-colorspace GRAY&qu=
ot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>colorspace/transparent: &quot;-colorspace =
Transparent&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>colorspace: &quot;-colorspace {1}&quot;</c=
ode></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>border: &quot;-border {1}x{2}&quot;</code>=
</td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>border/color: &quot;-bordercolor rgb({1},{=
2},{3})&quot;</code></td>
</tr>
<tr>
<td class=3D"confluenceTd"><code>border/width: &quot;-borderwidth {1}&quot;=
</code></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd"><code>flatten: &quot;-flatten&quot=
;</code></td>
</tr>
<tr>
<td colspan=3D"1" class=3D"confluenceTd"><p><code>resize: &quot;-resize {1}=
&quot;</code></p></td>
</tr>
</tbody>
</table>
</div>
<p>For more details on setting ImageMagick filters on image.ini please refe=
r to the <a href=3D"http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Refere=
nce/Configuration-files/image.ini/ImageMagick/Filters" class=3D"external-li=
nk" rel=3D"nofollow">[imagemagick] / filters</a> documentation.</p>
<h3 id=3D"Legacyconfigurationinjection-Extendinginjectedsettings">Extending=
 injected settings</h3>
<p>It's possible to add your own kernel event subscriber and also inject yo=
ur own settings by following how it is done in&nbsp;LegacyMapper\Configurat=
ion, and then at the end merge it with existing injected settings like so:<=
/p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeHeader panelHeader pdl" style=3D"border-bottom-width: 1px=
;">
<b>injected-settings</b>
</div>
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: php; gutter: false" style=3D"font-size=
:12px;">        $event-&gt;getParameters()-&gt;set(
            &quot;injected-settings&quot;,
            $settings + (array)$event-&gt;getParameters()-&gt;get( &quot;in=
jected-settings&quot; )
        );</pre>=20
</div>
</div>
<h2 id=3D"Legacyconfigurationinjection-InjectedBehavior">Injected&nbsp;Beha=
vior</h2>
<p>In addition to injected settings, some injection of&nbsp;behavior&nbsp;i=
s also&nbsp;performed.</p>
<h3 id=3D"Legacyconfigurationinjection-eZFormToken(CSRF)integration">eZForm=
Token (CSRF) integration</h3>=20
<div class=3D"aui-message hint shadowed information-macro">=20
<span class=3D"aui-icon icon-hint">Icon</span>=20
<div class=3D"message-content">=20
<p>This feature is only available as of eZ Publish 5.1 (2013.01)</p>=20
</div>=20
</div>=20
<p>If your config.yml setting have&nbsp;<em>framework.csrf_protection.enabl=
ed</em>&nbsp;set to <em>true</em>, then both&nbsp;<em>kernel.secret</em> an=
d&nbsp;<em>framework.csrf_protection.field_name</em> will be sent to ezxFor=
mToken class so csrf protection in legacy uses the same token and form fiel=
d name.</p>
<p>By making sure all your Symfony&nbsp;forms uses the provided&nbsp;<a hre=
f=3D"http://symfony.com/doc/current/book/forms.html#forms-csrf" class=3D"ex=
ternal-link" rel=3D"nofollow">csrf</a>&nbsp;<a href=3D"http://symfony.com/d=
oc/current/reference/forms/types/csrf.html" class=3D"external-link" rel=3D"=
nofollow">protection</a>, forms with <em>intention=3Dlegacy</em>&nbsp;can b=
e setup to send data to legacy kernel.</p>
<p>Please note that <code>framework.csrf_protection.field_name</code> shoul=
dn't be changed as that would prevent eZFormToken from working with most AJ=
AX custom code.</p>
<h3 id=3D"Legacyconfigurationinjection-ezpEvent(Cacheclear)integration">ezp=
Event (Cache clear) integration</h3>
<p>A&nbsp;listener&nbsp;is setup for both content/cache and content/cache/a=
ll to make sure Symfony (Internal proxy or Varnish with custom vcl) HTTP ca=
che is cleared when cache is cleared in eZ Publish admin interface.&nbsp;</=
p>
<p>&nbsp;</p>
    </div>
</body>
</html>
------=_Part_1427_1826952974.1413895904493--
