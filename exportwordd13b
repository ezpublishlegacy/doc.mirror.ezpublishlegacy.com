Message-ID: <1963861290.978.1413884635200.JavaMail.confluence@ip-10-127-227-192>
Subject: Exported From Confluence
MIME-Version: 1.0
Content-Type: multipart/related; 
	boundary="----=_Part_977_740817907.1413884635200"

------=_Part_977_740817907.1413884635200
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable
Content-Location: file:///C:/exported.html

<html xmlns:o=3D'urn:schemas-microsoft-com:office:office'
      xmlns:w=3D'urn:schemas-microsoft-com:office:word'
      xmlns:v=3D'urn:schemas-microsoft-com:vml'
      xmlns=3D'urn:w3-org-ns:HTML'>
<head>
    <meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Dutf-8=
">
    <title>ez_legacy_render_css</title>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:TargetScreenSize>1024x640</o:TargetScreenSize>
            <o:PixelsPerInch>72</o:PixelsPerInch>
            <o:AllowPNG/>
        </o:OfficeDocumentSettings>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
    <![endif]-->
    <style>
                <!--
        @page Section1 {
            size: 8.5in 11.0in;
            margin: 1.0in;
            mso-header-margin: .5in;
            mso-footer-margin: .5in;
            mso-paper-source: 0;
        }

        td {
            page-break-inside: avoid;
        }

        tr {
            page-break-after: avoid;
        }

        div.Section1 {
            page: Section1;
        }

        /* Confluence print stylesheet. Common to all themes for print medi=
a */
/* Full of !important until we improve batching for print CSS */

#main {
    padding-bottom: 1em !important; /* The default padding of 6em is too mu=
ch for printouts */
}

body {
    font-family: Arial, Helvetica, FreeSans, sans-serif;
    font-size: 10pt;
    line-height: 1.2;
}

body, #full-height-container, #main, #page, #content, .has-personal-sidebar=
 #content {
    background: #fff    !important;
    color: #000         !important;
    border: 0           !important;
    width: 100%         !important;
    height: auto        !important;
    min-height: auto    !important;
    margin: 0           !important;
    padding: 0          !important;
    display: block      !important;
}

a, a:link, a:visited, a:focus, a:hover, a:active {
    color: #000;
}
#content h1,
#content h2,
#content h3,
#content h4,
#content h5,
#content h6 {
    font-family: Arial, Helvetica, FreeSans, sans-serif;
    page-break-after: avoid;
}
pre {
    font-family: Monaco, "Courier New", monospace;
}

#header,
.aui-header-inner,
#navigation,
#sidebar,
.sidebar,
#personal-info-sidebar,
.ia-fixed-sidebar,
.page-actions,
.navmenu,
.ajs-menu-bar,
.noprint,
.inline-control-link,
.inline-control-link a,
a.show-labels-editor,
.global-comment-actions,
.comment-actions,
.quick-comment-container,
#addcomment {
    display: none !important;
}

.comment .date::before {
    content: none !important; /* remove middot for print view */
}

h1.pagetitle img {
    height: auto;
    width: auto;
}

.print-only {
    display: block;
}
#footer {
    position: relative !important; /* CONF-17506 Place the footer at end of=
 the content */
    margin: 0;
    padding: 0;
    background: none;
    clear: both;
}

#poweredby {
    border-top: none;
    background: none;
}

#poweredby li.print-only {
    display: list-item;
    font-style: italic;
}

#poweredby li.noprint {
    display:none;
}


/* no width controls in print */
.wiki-content .table-wrap,
.wiki-content p,
.panel .codeContent,
.panel .codeContent pre,
.image-wrap {
    overflow: visible !important;
}

/* TODO - should this work? */
#children-section,
#comments-section .comment,
#comments-section .comment .comment-body,
#comments-section .comment .comment-content,
#comments-section .comment p {
    page-break-inside: avoid;
}

#page-children a {
    text-decoration: none;
}

/**
 hide twixies

 the specificity here is a hack because print styles
 are getting loaded before the base styles. */
#comments-section.pageSection .section-header,
#comments-section.pageSection .section-title,
#children-section.pageSection .section-header,
#children-section.pageSection .section-title,
.children-show-hide {
    padding-left: 0;
    margin-left: 0;
}

.children-show-hide.icon {
    display: none;
}

/* personal sidebar */
.has-personal-sidebar #content {
    margin-right: 0px;
}

.has-personal-sidebar #content .pageSection {
    margin-right: 0px;
}
-->
    </style>
</head>
<body>
    <h1>ez_legacy_render_css</h1>
    <div class=3D"Section1">
        <p><style type=3D"text/css">/*<![CDATA[*/
div.rbtoc1413884635195 {padding: 0px;}
div.rbtoc1413884635195 ul {list-style: disc;margin-left: 0px;}
div.rbtoc1413884635195 li {margin-left: 0px;padding-left: 0px;}

/*]]>*/</style></p>
<div class=3D"toc-macro rbtoc1413884635195">=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#ez_legacy_render_css-Description">Description</a></li>=20
<li><a href=3D"#ez_legacy_render_css-PrototypeandArguments">Prototype and A=
rguments</a></li>=20
<li><a href=3D"#ez_legacy_render_css-Usage">Usage</a></li>=20
<li><a href=3D"#ez_legacy_render_css-Customization">Customization</a></li>=
=20
</ul>=20
</div>
<p></p>=20
<div class=3D"aui-message hint shadowed information-macro">=20
<p class=3D"title">Version compatibility</p>=20
<span class=3D"aui-icon icon-hint">Icon</span>=20
<div class=3D"message-content">=20
<p>This Twig helper is available as of eZ Publish <strong>5.3 / 2014.05</st=
rong></p>=20
</div>=20
</div>=20
<h2 id=3D"ez_legacy_render_css-Description">Description</h2>
<p><code>ez_legacy_render_css()</code> is a Twig helper which adds css (cod=
e and files) configured in ezjscore to legacy callback modules.</p>
<p>It returns a HTML code to insert in the &lt;head&gt; of your page.</p>
<h2 id=3D"ez_legacy_render_css-PrototypeandArguments">Prototype and Argumen=
ts</h2>
<p><code><strong>ez_legacy_render_cs(</strong><strong>)</strong></code></p>
<h2 id=3D"ez_legacy_render_css-Usage">Usage</h2>
<p>Just insert the function where the HTML code needs to be inserted.</p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: html/xml; gutter: false" style=3D"font=
-size:12px;">&lt;head&gt;
=09&lt;!-- code of your head --&gt;

=09{# Adds css code and files from ezjscore #}
=09{{ ez_legacy_render_css() }}

=09&lt;!-- code of your head --&gt;
&lt;/head&gt;</pre>=20
</div>
</div>
<h2 id=3D"ez_legacy_render_css-Customization">Customization</h2>
<p>You can customize the display of the function by overriding the template=
 used in the following parameter: ezpublish_legacy.twig.extension.template.=
css</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
    </div>
</body>
</html>
------=_Part_977_740817907.1413884635200--
