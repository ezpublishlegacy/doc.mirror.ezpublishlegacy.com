Message-ID: <608918930.1448.1413895961561.JavaMail.confluence@ip-10-127-227-192>
Subject: Exported From Confluence
MIME-Version: 1.0
Content-Type: multipart/related; 
	boundary="----=_Part_1447_560321575.1413895961561"

------=_Part_1447_560321575.1413895961561
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable
Content-Location: file:///C:/exported.html

<html xmlns:o=3D'urn:schemas-microsoft-com:office:office'
      xmlns:w=3D'urn:schemas-microsoft-com:office:word'
      xmlns:v=3D'urn:schemas-microsoft-com:vml'
      xmlns=3D'urn:w3-org-ns:HTML'>
<head>
    <meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Dutf-8=
">
    <title>How to implement a Custom Tag for XMLText FieldType</title>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:TargetScreenSize>1024x640</o:TargetScreenSize>
            <o:PixelsPerInch>72</o:PixelsPerInch>
            <o:AllowPNG/>
        </o:OfficeDocumentSettings>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
    <![endif]-->
    <style>
                <!--
        @page Section1 {
            size: 8.5in 11.0in;
            margin: 1.0in;
            mso-header-margin: .5in;
            mso-footer-margin: .5in;
            mso-paper-source: 0;
        }

        td {
            page-break-inside: avoid;
        }

        tr {
            page-break-after: avoid;
        }

        div.Section1 {
            page: Section1;
        }

        /* Confluence print stylesheet. Common to all themes for print medi=
a */
/* Full of !important until we improve batching for print CSS */

#main {
    padding-bottom: 1em !important; /* The default padding of 6em is too mu=
ch for printouts */
}

body {
    font-family: Arial, Helvetica, FreeSans, sans-serif;
    font-size: 10pt;
    line-height: 1.2;
}

body, #full-height-container, #main, #page, #content, .has-personal-sidebar=
 #content {
    background: #fff    !important;
    color: #000         !important;
    border: 0           !important;
    width: 100%         !important;
    height: auto        !important;
    min-height: auto    !important;
    margin: 0           !important;
    padding: 0          !important;
    display: block      !important;
}

a, a:link, a:visited, a:focus, a:hover, a:active {
    color: #000;
}
#content h1,
#content h2,
#content h3,
#content h4,
#content h5,
#content h6 {
    font-family: Arial, Helvetica, FreeSans, sans-serif;
    page-break-after: avoid;
}
pre {
    font-family: Monaco, "Courier New", monospace;
}

#header,
.aui-header-inner,
#navigation,
#sidebar,
.sidebar,
#personal-info-sidebar,
.ia-fixed-sidebar,
.page-actions,
.navmenu,
.ajs-menu-bar,
.noprint,
.inline-control-link,
.inline-control-link a,
a.show-labels-editor,
.global-comment-actions,
.comment-actions,
.quick-comment-container,
#addcomment {
    display: none !important;
}

.comment .date::before {
    content: none !important; /* remove middot for print view */
}

h1.pagetitle img {
    height: auto;
    width: auto;
}

.print-only {
    display: block;
}
#footer {
    position: relative !important; /* CONF-17506 Place the footer at end of=
 the content */
    margin: 0;
    padding: 0;
    background: none;
    clear: both;
}

#poweredby {
    border-top: none;
    background: none;
}

#poweredby li.print-only {
    display: list-item;
    font-style: italic;
}

#poweredby li.noprint {
    display:none;
}


/* no width controls in print */
.wiki-content .table-wrap,
.wiki-content p,
.panel .codeContent,
.panel .codeContent pre,
.image-wrap {
    overflow: visible !important;
}

/* TODO - should this work? */
#children-section,
#comments-section .comment,
#comments-section .comment .comment-body,
#comments-section .comment .comment-content,
#comments-section .comment p {
    page-break-inside: avoid;
}

#page-children a {
    text-decoration: none;
}

/**
 hide twixies

 the specificity here is a hack because print styles
 are getting loaded before the base styles. */
#comments-section.pageSection .section-header,
#comments-section.pageSection .section-title,
#children-section.pageSection .section-header,
#children-section.pageSection .section-title,
.children-show-hide {
    padding-left: 0;
    margin-left: 0;
}

.children-show-hide.icon {
    display: none;
}

/* personal sidebar */
.has-personal-sidebar #content {
    margin-right: 0px;
}

.has-personal-sidebar #content .pageSection {
    margin-right: 0px;
}
-->
    </style>
</head>
<body>
    <h1>How to implement a Custom Tag for XMLText FieldType</h1>
    <div class=3D"Section1">
        <div class=3D"aui-message warning shadowed information-macro">=20
<p class=3D"title">Version compatibility</p>=20
<span class=3D"aui-icon icon-warning">Icon</span>=20
<div class=3D"message-content">=20
<p>This recipe is compatible with <strong>eZ Publish 5.1 / 2013.05</strong>=
</p>=20
</div>=20
</div>=20
<h1 id=3D"HowtoimplementaCustomTagforXMLTextFieldType-Customtags">Custom ta=
gs</h1>
<p>As of eZ Publish 5.2 <strong>XMLText fieldtype</strong> supports a limit=
ed number of tags in its internal eZXML format to render HTML5. However, it=
 is possible to extend the rendering by implementing <strong>custom tags</s=
trong>.</p>
<p>As HTML5 rendering in eZ Publish is done through <a href=3D"http://en.wi=
kipedia.org/wiki/XSLT" class=3D"external-link" rel=3D"nofollow">XSLT</a>, y=
ou will need to create an XSL stylesheet to extend the rendering.</p>=20
<div class=3D"aui-message hint shadowed information-macro">=20
<p class=3D"title">Note on legacy custom tags</p>=20
<span class=3D"aui-icon icon-hint">Icon</span>=20
<div class=3D"message-content">=20
<p>To be able to edit a custom tag from admin interface, you'll still need =
to <a href=3D"http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Da=
tatypes/XML-block/Custom-tags" class=3D"external-link" rel=3D"nofollow">reg=
ister your custom tag in the legacy kernel</a> (at least the configuration =
part, template not being mandatory for edition).</p>=20
</div>=20
</div>=20
<p><br /><style type=3D"text/css">/*<![CDATA[*/
div.rbtoc1413895961544 {padding: 0px;}
div.rbtoc1413895961544 ul {list-style: disc;margin-left: 0px;}
div.rbtoc1413895961544 li {margin-left: 0px;padding-left: 0px;}

/*]]>*/</style></p>
<div class=3D"toc-macro rbtoc1413895961544">=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#HowtoimplementaCustomTagforXMLTextFieldType-Customtags">Cus=
tom tags</a>=20
<ul class=3D"toc-indentation">=20
<li><a href=3D"#HowtoimplementaCustomTagforXMLTextFieldType-Registeryourcus=
tomXSLstylesheet">Register your custom XSL stylesheet</a></li>=20
</ul> </li>=20
</ul>=20
</div>
<p></p>
<h2 id=3D"HowtoimplementaCustomTagforXMLTextFieldType-RegisteryourcustomXSL=
stylesheet">Register your custom XSL stylesheet</h2>
<p>For eZ Publish 5.1, there's a possible workaround that uses <code>eZXml2=
Html5.xsl</code> as a starting point, and require the following steps:</p>
<ol>
<li>Copy the three files <code>eZXml2Html5.xsl</code>, <code>eZXml2Html5_co=
re.xsl</code> and <code>eZXml2Html5_custom.xsl</code> files from <code>vend=
or/ezsystems/ezpublish-kernel/eZ/Publish/Core/FieldType/XmlText/Input/Resou=
rces/stylesheets/</code> to your bundle</li>
<li><p>Edit your main file,&nbsp;<code>eZXml2Html5.xsl</code> in our exampl=
e, to something like the following:</p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeHeader panelHeader pdl" style=3D"border-bottom-width: 1px=
;">
<b>eZXml2Html5.xsl</b>
</div>
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: html/xml; gutter: false" style=3D"font=
-size:12px;">&lt;?xml version=3D&quot;1.0&quot; encoding=3D&quot;UTF-8&quot=
;?&gt;
=09&lt;xsl:stylesheet=20
=09=09=09version=3D&quot;1.0&quot;=20
=09=09=09xmlns:xsl=3D&quot;http://www.w3.org/1999/XSL/Transform&quot;=20
=09=09=09xmlns:xhtml=3D&quot;http://ez.no/namespaces/ezpublish3/xhtml/&quot=
;=20
=09=09=09xmlns:custom=3D&quot;http://ez.no/namespaces/ezpublish3/custom/&qu=
ot;=20
=09=09=09xmlns:image=3D&quot;http://ez.no/namespaces/ezpublish3/image/&quot=
;=20
=09=09=09exclude-result-prefixes=3D&quot;xhtml custom image&quot;&gt;

=09&lt;xsl:import href=3D&quot;eZXml2Html5_core.xsl&quot; /&gt;
=09&lt;xsl:import href=3D&quot;eZXml2Html5_custom.xsl&quot; /&gt;
=09&lt;xsl:output method=3D&quot;html&quot; indent=3D&quot;yes&quot; encodi=
ng=3D&quot;UTF-8&quot; /&gt;
&lt;/xsl:stylesheet&gt;</pre>=20
</div>
</div></li>
<li><p>Edit your <em>_custom</em> file, <code><code>eZXml2Html5</code>_cust=
om.xsl</code> in our example, and add the content you wish, like in the fol=
lowing example:</p>
<div class=3D"code panel pdl" style=3D"border-width: 1px;">
<div class=3D"codeHeader panelHeader pdl" style=3D"border-bottom-width: 1px=
;">
<b>eZXml2Html5_custom.xsl</b>
</div>
<div class=3D"codeContent panelContent pdl">=20
<pre class=3D"theme: Eclipse; brush: html/xml; gutter: false" style=3D"font=
-size:12px;">&lt;?xml version=3D&quot;1.0&quot; encoding=3D&quot;UTF-8&quot=
;?&gt;
=09&lt;xsl:stylesheet
        version=3D&quot;1.0&quot;
        xmlns:xsl=3D&quot;http://www.w3.org/1999/XSL/Transform&quot;
        xmlns:xhtml=3D&quot;http://ez.no/namespaces/ezpublish3/xhtml/&quot;
        xmlns:custom=3D&quot;http://ez.no/namespaces/ezpublish3/custom/&quo=
t;
        xmlns:image=3D&quot;http://ez.no/namespaces/ezpublish3/image/&quot;
        exclude-result-prefixes=3D&quot;xhtml custom image&quot;&gt;
=20
    &lt;xsl:output method=3D&quot;html&quot; indent=3D&quot;yes&quot; encod=
ing=3D&quot;UTF-8&quot;/&gt;
=20
    &lt;!-- Template below will match the following custom tag: --&gt;
    &lt;!-- &lt;custom name=3D&quot;youtube&quot; custom:video=3D&quot;//ww=
w.youtube.com/embed/MfOnq-zXXBw&quot; custom:videoWidth=3D&quot;640&quot; c=
ustom:videoHeight=3D&quot;380&quot;/&gt; --&gt;
    &lt;xsl:template match=3D&quot;custom[@name=3D'youtube']&quot;&gt;
        &lt;div class=3D&quot;jvembed jvembed-youtube&quot;&gt;
            &lt;iframe&gt;
                &lt;xsl:attribute name=3D&quot;width&quot;&gt;&lt;xsl:value=
-of select=3D&quot;@custom:videoWidth&quot;/&gt;&lt;/xsl:attribute&gt;
                &lt;xsl:attribute name=3D&quot;height&quot;&gt;&lt;xsl:valu=
e-of select=3D&quot;@custom:videoHeight&quot;/&gt;&lt;/xsl:attribute&gt;
                &lt;xsl:attribute name=3D&quot;src&quot;&gt;&lt;xsl:value-o=
f select=3D&quot;@custom:video&quot;/&gt;&lt;/xsl:attribute&gt;
                &lt;xsl:attribute name=3D&quot;frameborder&quot;&gt;0&lt;/x=
sl:attribute&gt;
                &lt;xsl:attribute name=3D&quot;allowfullscreen&quot;/&gt;
            &lt;/iframe&gt;
        &lt;/div&gt;
    &lt;/xsl:template&gt;
&lt;/xsl:stylesheet&gt;</pre>=20
</div>
</div></li>
<li><p>Add the path where your .xsl files were placed to <code>ezpublish/co=
nfig/parameters.yml</code>, for example:</p>
<div class=3D"preformatted panel" style=3D"border-width: 1px;">
<div class=3D"preformattedContent panelContent">=20
<pre>parameters:
    ezpublish.fieldType.ezxmltext.converter.html5.resources: %kernel.root_d=
ir%/../src/MyBundle/Resources/eZXml2Html5.xsl</pre>=20
</div>
</div></li>
</ol>=20
<div class=3D"aui-message warning shadowed information-macro">=20
<span class=3D"aui-icon icon-warning">Icon</span>=20
<div class=3D"message-content">=20
<p>Please note that in the <code><code>eZXml2Html5</code>_custom.xsl</code>=
 file you can add as many custom tags you wish, instead of repeating these =
steps or adding independent files for each tag.</p>=20
</div>=20
</div>=20
<p><br /><br /></p>
    </div>
</body>
</html>
------=_Part_1447_560321575.1413895961561--
